import { Component, ElementRef } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {MoviesDataService} from "./movies-data.service";
import touchy from "touchy";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private hideMovie: boolean = false;

  constructor(
    private http: HttpClient,
    private moviesDataService: MoviesDataService,
    private elRef: ElementRef
  ) {}

  ngOnInit(): void {
    this.detectSwipe();

    this.http.get('/assets/data.json').subscribe((data: any[]) => {
      this.moviesDataService.setData(data);
    });

    this.moviesDataService.currentMovieIndex.subscribe((index) => {
      if (this.moviesDataService.getData().length && index > this.moviesDataService.getData().length - 1) {
        setTimeout(() => {
          this.hideMovie = true;
        }, 500);
      }
    })
  }

  detectSwipe() {
    touchy.enableOn(document);
    let appRoot = this.elRef.nativeElement;
    appRoot.addEventListener( 'swipe:left', () => {
      this.moviesDataService.accept();
    }, false);
    appRoot.addEventListener( 'swipe:right', () => {
      this.moviesDataService.reject();
    }, false);
  }
}
