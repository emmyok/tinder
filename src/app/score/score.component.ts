import { Component, OnInit } from '@angular/core';
import {MoviesDataService} from "../movies-data.service";

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {

  private score:string="";

  constructor(
    private moviesDataService: MoviesDataService
  ) { }

  ngOnInit() {
    this.moviesDataService.currentMoviesData.subscribe(data=>{
      if(data.length){
        this.score= data[this.moviesDataService.getCurrentMovieIndex()].rating;
      }
    })
    this.moviesDataService.currentMovieIndex.subscribe(index=>{
      if(index && index < this.moviesDataService.getData().length){
        this.score=this.moviesDataService.getData()[index].rating;
      }
    })

  }

}
