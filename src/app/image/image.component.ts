import {Component, OnInit} from '@angular/core';
import {MoviesDataService} from "../movies-data.service";

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {
  private image: string = "";
  private className: string = "";

  constructor(
    private moviesDataService: MoviesDataService
  ) {
  }

  ngOnInit() {
    this.moviesDataService.currentMoviesData.subscribe(data => {
      if (data.length) {
        this.image = data[this.moviesDataService.getCurrentMovieIndex()].imageURL;
      }
    });

    this.moviesDataService.currentMovieIndex.subscribe(index => {
      if (index) {
        this.className = this.moviesDataService.rejected ? 'reject' : 'accept';
      }

      if (index && index < this.moviesDataService.getData().length) {
        setTimeout(() => {
          this.className = '';
          this.image = this.moviesDataService.getData()[index].imageURL;
        }, 500);
      }
    })


  }


}
