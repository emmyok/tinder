import { Component, OnInit } from '@angular/core';
import {MoviesDataService} from "../movies-data.service";

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {
  private title: string = '';

  constructor(
    private moviesDataService: MoviesDataService
  ) { }

  ngOnInit() {
    this.moviesDataService.currentMoviesData.subscribe((data) => {
      if (data.length) {
        this.title = data[this.moviesDataService.getCurrentMovieIndex()].title;
      }
    });

    this.moviesDataService.currentMovieIndex.subscribe(index=>{
      if(index && index < this.moviesDataService.getData().length){
        this.title=this.moviesDataService.getData()[index].title;
      }

    })
  }

}
