import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { TitleComponent } from './title/title.component';
import { ImageComponent } from './image/image.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { ScoreComponent } from './score/score.component';
import { SummaryComponent } from './summary/summary.component';

import { HttpClientModule } from '@angular/common/http';
import {MoviesDataService} from "./movies-data.service";


@NgModule({
  declarations: [
    AppComponent,
    TitleComponent,
    ImageComponent,
    ButtonsComponent,
    ScoreComponent,
    SummaryComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    MoviesDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
