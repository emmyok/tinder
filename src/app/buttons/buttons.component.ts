import {Component, OnInit} from '@angular/core';
import {MoviesDataService} from "../movies-data.service";

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss']
})
export class ButtonsComponent implements OnInit {
  private isReadyToChange = true;
  private animationTime = 1000;

  constructor(private moviesDataService: MoviesDataService) {
  }

  ngOnInit() {

  }

  reject() {
    if (this.isReadyToChange) {
      this.isReadyToChange = false;
      this.moviesDataService.reject();
      setTimeout(() => {
        this.isReadyToChange = true;
      }, this.animationTime);
    }
  }

  accept() {
    if (this.isReadyToChange) {
      this.isReadyToChange = false;
      this.moviesDataService.accept();
      setTimeout(() => {
        this.isReadyToChange = true;
      }, this.animationTime);
    }
  }

}
