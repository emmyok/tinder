import { Component, OnInit } from '@angular/core';
import {MoviesDataService} from "../movies-data.service";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  private summary:string="";

  constructor(
    private moviesDataService:MoviesDataService
  ) {


  }

  ngOnInit() {
    this.moviesDataService.currentMoviesData.subscribe((data) => {
      if(data.length){
        this.summary=data[this.moviesDataService.getCurrentMovieIndex()].summary;
      }
    });
    this.moviesDataService.currentMovieIndex.subscribe(index=>{
      if(index && index < this.moviesDataService.getData().length){
        this.summary= this.moviesDataService.getData()[index].summary;
      }

      }
    )

  }

}
