import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/toPromise';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class MoviesDataService {
  private moviesData = new BehaviorSubject<any[]>([]);
  currentMoviesData = this.moviesData.asObservable();

  private movieIndex = new BehaviorSubject<number>(0);
  currentMovieIndex = this.movieIndex.asObservable();

  rejected: boolean = false;

  constructor(
    private http: HttpClient
  ) {}

  setData(data: any[]) {
    this.moviesData.next(data);
  }

  getData(){
    return this.moviesData.value;
  }

  updateMovieIndex() {
    this.movieIndex.next(this.getCurrentMovieIndex() + 1);
  }

  getCurrentMovieIndex() {
    return this.movieIndex.value;
  }

  reject() {
    this.rejected = true;
    let id = this.getData()[this.getCurrentMovieIndex()].id;
    this.http.put('/recommendations/' + id + '/reject', {}).subscribe((data: any[]) => {
      this.setData(data);
    });
    this.updateMovieIndex();
  }

  accept() {
    this.rejected = false;
    let id = this.getData()[this.getCurrentMovieIndex()].id;
    this.http.put('/recommendations/' + id + '/accept', {}).subscribe((data: any[]) => {
      this.setData(data);
    });
    this.updateMovieIndex();
  }


}
