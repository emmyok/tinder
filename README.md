# Tinder

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).




## TASK REQUIREMENTS
Objective: create a single-page application for selecting a movie to watch using your phone.

Functional​ ​description
● Assume you have a backend that returns the following data structure on GET on
/recommendations
[{
id: “1and3011”,
imageURL:
“https://pl.wikipedia.org/wiki/Gwiezdne_wojny:_Przebudzenie_Mocy#/media/File:R2D2_(222135
48240).jpg”,
title: “Inferno”,
summary: “Lorem ipsum....”,
rating: 5.3}, {id: “2301abc”,
imageURL:
“https://upload.wikimedia.org/wikipedia/commons/f/f1/SWCA_-_Stormtrooper_from_Force_Awa
kens_%2817202865375%29.jpg”,
title: “Star Wars: Episode VII - The Force Awakens”,
summary: “Lorem ipsum....”,
rating 8.2}
]

(feel free to add more items)
● The application should show the movie titles, image, summary and rating.
● The user can accept or reject the suggestion. Accepting is done by green button. Rejecting is
done by red button or swipe.
● The backend gets notified on user’s decision by a PUT call to /recommendations/<id>/accept
or /recommendations/<id>/reject
URL.

Technical​ ​requirements
● Needs to be structured like any other modern javascript application
● Needs to look and work ok on the mobile phone (Android with Chrome) and normal
browser (Chrome)
● Framework:
○ React or Angular
○ Redux
● SASS